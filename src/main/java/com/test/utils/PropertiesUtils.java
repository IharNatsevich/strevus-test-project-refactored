package com.test.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertiesUtils {

	private HashMap<String, String> properties;

	public HashMap<String, String> getProperties() {
		if (properties == null) {
			properties = loadFromFile();
		}
		return properties;
	}

	private HashMap<String, String> loadFromFile() {
		HashMap<String, String> result = new HashMap<String, String>();
		// FIXME rename vars !!!!!!
		Properties props = new Properties();
		String path = (System.getProperty(Constants.USER_DIR) + "\\src\\")
				+ Constants.PROP_FILE_PATH;
		FileInputStream fis;
		try {
			fis = new FileInputStream(new File(path));
			props.load(fis);
			fis.close();

			String ctrl = props.getProperty(Constants.PROP_CONST_TOREAD);
			result.put(Constants.PROP_CONST_TOREAD, clearPath(ctrl));
			ctrl = props.getProperty(Constants.PROP_CONST_TOWRITE);
			result.put(Constants.PROP_CONST_TOWRITE, clearPath(ctrl));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	private String clearPath(String path) {

		if (path.contains(Constants.VAR_BASE_DIR)) {
			path = path.replace(Constants.VAR_BASE_DIR,
					System.getProperty(Constants.USER_DIR));
		}
		return path;
	}

	// FIXME out of here
	public void printMap(Map<?, ?> map) {
		StringBuilder sb = new StringBuilder(128);
		sb.append("{");
		for (Map.Entry<?, ?> entry : map.entrySet()) {
			if (sb.length() > 1) {
				sb.append(", ");
			}
			sb.append(entry.getKey()).append("=").append(entry.getValue());
		}
		sb.append("}");
		System.out.println(sb);
	}

//	public String getAbsolutePath() {
//		return PropertiesUtils.class.getProtectionDomain().getCodeSource()
//				.getLocation().toString();
//	}

}
