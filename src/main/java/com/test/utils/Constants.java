package com.test.utils;

import java.util.HashMap;

public class Constants {

	public static final String PROP_FILE_PATH = "/testapp.properties";

	public static final String OpenBracket = "(";
	public static final String CloseBracket = ")";

	public static final String PROP_CONST_TOREAD = "PathToRead";
	public static final String PROP_CONST_TOWRITE = "PathToWrite";
	public static final String USER_DIR = "user.dir";

	public static final String VAR_BASE_DIR = "{basedir}";

	private static String[] operations = { "+", "-", "/", "*", "^" };
	private static String[] functions = { "sqrt", "sin", "cos", "tg", "log" };

	private static HashMap<String, Integer> priority = new HashMap<String, Integer>() {

		{
			put(getSummToken(), 1);
			put(getSubToken(), 1);
			put(getMultToken(), 2);
			put(getDivToken(), 2);
			put(getPowToken(), 3);
		}
	};

	public static HashMap<String, Integer> getPriorityMap() {
		return priority;
	}

	public static String[] getOperations() {
		return operations;
	}

	public static String[] getFunctions() {
		return functions;
	}

	public static String getSqrtToken() {
		return getFunctions()[0];
	}

	public static String getSinToken() {
		return getFunctions()[1];
	}

	public static String getCosToken() {
		return getFunctions()[2];
	}

	public static String getTgToken() {
		return getFunctions()[3];
	}

	public static String getLogToken() {
		return getFunctions()[4];
	}

	public static String getSummToken() {
		return getOperations()[0];
	}

	public static String getSubToken() {
		return getOperations()[1];
	}

	public static String getDivToken() {
		return getOperations()[2];
	}

	public static String getMultToken() {
		return getOperations()[3];
	}

	public static String getPowToken() {
		return getOperations()[4];
	}

	// FIXME remove out of constants class
	public static String createDelimeterStr() {
		String str = OpenBracket + CloseBracket;
		for (int i = 0; i < operations.length; i++) {
			str += operations[i];
		}

		return str;

	}

}
