package com.test.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class FileWorker {

    public void write(String filePath, String output) {
        PrintWriter out = null;
        try {
            out = new PrintWriter(new FileOutputStream(filePath));
            out.println(output);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    public List<String> read(String filePath) throws FileNotFoundException {
        List<String> result = new ArrayList<String>();
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(filePath));
            String s = in.readLine();
            while (s != null) {
                result.add(s);
                s = in.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

}
