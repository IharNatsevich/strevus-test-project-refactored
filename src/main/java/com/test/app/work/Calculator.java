package com.test.app.work;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import com.test.api.Factory;
import com.test.api.Function;
import com.test.api.Operation;
import com.test.api.factory.impl.FunctionsFactoryImpl;
import com.test.api.factory.impl.OperationsFactoryImpl;

public class Calculator {
	private TokensChecker tokensChecker;
	private Stack<String> operationStack;
	private List<String> outputString;

	public Calculator() {
		this.tokensChecker = new TokensChecker();
		this.operationStack = new Stack<String>();
		this.outputString = new ArrayList<String>();
	}

	public void setOperationStack(Stack<String> stack) {
		this.operationStack = stack;
	}

	public void setOutPutString(List<String> str) {
		this.outputString = str;
	}

	public Double calculate() {
		Double result = 0d;

		while (tokensChecker.isOperationsExist(outputString)) {
			int index = getFirstOperationIndex();

			String func = null;
            String arg = null;
			if (tokensChecker.isFunction(outputString.get(index - 1))) {
				func = outputString.get(index - 1);
				arg = outputString.get(index - 2);
			} else {
				if (tokensChecker.isFunction(outputString.get(index - 2))) {
					func = outputString.get(index - 2);
					arg = outputString.get(index - 1);
				}
			}

			if (func != null && arg != null) {
				result = processFunction(func, arg, index);
			} else {
				result = processOperation(outputString.get(index - 2),
						outputString.get(index - 1), index);
			}
		}

		if (tokensChecker.isFunctionExist(outputString)
				&& !tokensChecker.isOperationsExist(outputString)) {
			result = processLastFunctions();
		}

		return result;
	}

	private Double processLastFunctions() {
		Double result = 0d;
		while (tokensChecker.isFunctionExist(outputString)) {
			int index = getInsideFunctionIndex();

			String func = null, arg = null;
			if (tokensChecker.isFunction(outputString.get(index))) {
				func = outputString.get(index);
				arg = outputString.get(index + 1);
			} else {
				if (tokensChecker.isFunction(outputString.get(index + 1))) {
					func = outputString.get(index + 1);
					arg = outputString.get(index);
				}
			}

			if (func != null && arg != null) {
				result = processFunction(func, arg, index + 2);
			}
		}
		return result;
	}

	private Double processOperation(String op1, String op2, int index) {
		Factory<Operation> opFactory = new OperationsFactoryImpl();
		Operation operation = opFactory.getItem(outputString.get(index));
		Double result = operation.calculate(op1, op2);
		shiftOutputString(index, result, false);
		return result;
	}

	private Double processFunction(String func, String arg, int index) {
		Factory<Function> funcFactory = new FunctionsFactoryImpl();
		Function function = funcFactory.getItem(func);
        Double result = function.calculate(arg);
		shiftOutputString(index, result, true);
		return result;
	}

	private void shiftOutputString(int index, Double result, boolean isFunctionCalc) {
		if (!isFunctionCalc) {
			outputString.remove(index);
		}
		outputString.remove(index - 1);
		outputString.remove(index - 2);
		outputString.add(index - 2, String.valueOf(result));
	}

	private int getFirstOperationIndex() {
		int result = -1;
		for (int i = 0; i < outputString.size(); i++) {
			if (tokensChecker.isOperation(outputString.get(i))) {
				result = i;
				return result;
			}
		}
		return result;
	}

	private int getInsideFunctionIndex() {
		int result = -1;
		for (int i = 0; i < outputString.size(); i++) {
			if (tokensChecker.isFunction(outputString.get(i))) {
				result = i;
			}
		}
		return result;
	}

}
