package com.test.app.work;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

import com.test.utils.Constants;

public class Parser {
	private TokensChecker tokensChecker;
	private Stack<String> operationStack;
	private List<String> outputString;

	public Parser() {
		this.tokensChecker = new TokensChecker();
		this.operationStack = new Stack<String>();
		this.outputString = new ArrayList<String>();
	}

	public Stack<String> getOperationStack() {
		return this.operationStack;
	}

	public List<String> getOutPutString() {
		return this.outputString;
	}

	public void parseToRPN(String inputString) throws IllegalArgumentException {

		clearData();
		inputString = clearInputStr(inputString);

		StringTokenizer tokenizer = new StringTokenizer(inputString,
				Constants.createDelimeterStr(), true);

		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken();
			if (tokensChecker.isNumber(token) || tokensChecker.isFunction(token)) {
                outputString.add(token);
			} else if (tokensChecker.isOperation(token)) {
				changePlaceInStack(token);
			} else if (tokensChecker.isOpenBracket(token)) {
				operationStack.push(token);
			} else if (tokensChecker.isCloseBracket(token)) {
				popOperationsFromStack();
			} else {
				throw new IllegalArgumentException("Incorrect input");
			}
		}
		popLastOperationsFromStack();

	}

	private void changePlaceInStack(String token) {
		while (!operationStack.isEmpty()
				&& tokensChecker.isOperation(operationStack.lastElement())
				&& tokensChecker.isPriorityHigher(Constants.getPriorityMap()
						.get(operationStack.lastElement()), Constants
						.getPriorityMap().get(token))) {
			outputString.add(operationStack.pop());
		}
		operationStack.push(token);
	}

	private void popOperationsFromStack() {
		while (!operationStack.isEmpty() && !tokensChecker.isOpenBracket(operationStack.lastElement())) {
			outputString.add(operationStack.pop());
		}
		operationStack.pop();

	}

	private void popLastOperationsFromStack() {
		while (!operationStack.isEmpty()) {
			outputString.add(operationStack.pop());

		}
	}

	private void clearData() {
		operationStack.clear();
		outputString.clear();
	}

	private String clearInputStr(String input) {
		String result = input.replace(" ", "").replace("(-", "(0-")
				.replace(",-", ",0-");
//		if (input.charAt(0) == '-') {
//			input = "0" + input;
//		}
		return result;
	}

}
