package com.test.app.work;

import java.util.List;

import com.test.utils.Constants;

public class TokensChecker {

	public boolean isNumber(String token) {
		boolean result = true;
		try {
			Double.parseDouble(token);
		} catch (Exception e) {
			result = false;
		}

		return result;
	}

	public boolean isFunction(String token) {
		boolean result = false;
		for (String function : Constants.getFunctions()) {
			if (token.equalsIgnoreCase(function)) {
				result = true;
			}

		}
		return result;
	}

	public boolean isOperation(String token) {
		boolean result = false;
		for (String operation : Constants.getOperations()) {
			if (token.equalsIgnoreCase(operation)) {
				result = true;
			}
		}
		return result;
	}

	public boolean isOpenBracket(String token) {
		boolean result = false;
		if (token.equalsIgnoreCase(Constants.OpenBracket)) {
			result = true;
		}
		return result;
	}

	public boolean isCloseBracket(String token) {
		boolean result = false;
		if (token.equalsIgnoreCase(Constants.CloseBracket)) {
			result = true;
		}
		return result;
	}

	public boolean isPriorityHigher(Integer pr1, Integer pr2) {
		boolean result = false;
		if (pr1 >= pr2) {
			result = true;
		} else {
			result = false;
		}
		return result;
	}

	public boolean isOperationsExist(List<String> outputString) {
		boolean result = false;
		for (String operation : Constants.getOperations()) {
			for (String token : outputString) {
				if (operation.equals(token)) {
					result = true;
				}
			}

		}
		return result;
	}

	public boolean isFunctionExist(List<String> outputString) {
		boolean result = false;
		for (String function : Constants.getFunctions()) {
			for (String token : outputString) {
				if (function.equals(token)) {
					result = true;
				}
			}

		}
		return result;
	}

}
