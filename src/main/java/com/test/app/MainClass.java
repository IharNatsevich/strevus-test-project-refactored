package com.test.app;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import com.test.api.Factory;
import com.test.api.Operation;
import com.test.api.factory.impl.OperationsFactoryImpl;
import com.test.app.work.Calculator;
import com.test.app.work.Parser;
import com.test.utils.Constants;
import com.test.utils.FileWorker;
import com.test.utils.PropertiesUtils;

public class MainClass {

    public static void main(String[] args) {

        List<String> vars = readVarsToCalc();
        Parser parser = new Parser();
        Calculator calculator = new Calculator();
        FileWorker fileWorker = new FileWorker();
        PropertiesUtils propertiesUtils = new PropertiesUtils();
        for (String inputString : vars) {
            parser.parseToRPN(inputString);
            calculator.setOperationStack(parser.getOperationStack());
            calculator.setOutPutString(parser.getOutPutString());
            String result = String.valueOf(calculator.calculate());
            fileWorker.write(
                    propertiesUtils.getProperties().get(
                            Constants.PROP_CONST_TOWRITE), inputString + " = "
                    + result);
            System.out.println(inputString + " = " + result);

        }

    }

    private static List<String> readVarsToCalc() {
        FileWorker fileWorker = new FileWorker();
        PropertiesUtils propertiesUtils = new PropertiesUtils();
        List<String> result = new ArrayList<String>();
        try {
            result = fileWorker.read(propertiesUtils.getProperties().get(
                    Constants.PROP_CONST_TOREAD));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return result;
    }

}
