package com.test.api;

public interface Function {

	public Double calculate(String argument);

}
