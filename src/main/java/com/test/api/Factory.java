package com.test.api;

public interface Factory<T> {

	public T getItem(String token);
}
