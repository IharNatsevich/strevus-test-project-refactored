package com.test.api.operations.impl;

import com.test.api.Operation;

public class SubOperationImpl implements Operation {

	@Override
	public Double calculate(String op1, String op2) {
		return Double.parseDouble(op1) - Double.parseDouble(op2);
	}

}
