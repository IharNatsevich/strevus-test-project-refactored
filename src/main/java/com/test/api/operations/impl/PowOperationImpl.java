package com.test.api.operations.impl;

import com.test.api.Operation;

public class PowOperationImpl implements Operation {

	@Override
	public Double calculate(String op1, String op2) {
		return Math.pow(Double.parseDouble(op1), Double.parseDouble(op2));
	}

}
