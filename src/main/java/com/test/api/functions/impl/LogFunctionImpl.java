package com.test.api.functions.impl;

import com.test.api.Function;

public class LogFunctionImpl implements Function {

	@Override
	public Double calculate(String argument) {

		return Math.log(Double.parseDouble(argument));
	}

}
