package com.test.api.functions.impl;

import com.test.api.Function;

public class SinFunctionImpl implements Function {

	@Override
	public Double calculate(String argument) {

		return Math.sin(Double.parseDouble(argument));
	}

}
