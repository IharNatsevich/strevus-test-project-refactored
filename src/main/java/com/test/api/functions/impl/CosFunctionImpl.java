package com.test.api.functions.impl;

import com.test.api.Function;

public class CosFunctionImpl implements Function {

	@Override
	public Double calculate(String argument) {

		return Math.cos(Double.parseDouble(argument));
	}

}
