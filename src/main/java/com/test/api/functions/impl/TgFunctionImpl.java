package com.test.api.functions.impl;

import com.test.api.Function;

public class TgFunctionImpl implements Function {

	@Override
	public Double calculate(String argument) {

		return Math.tan(Double.parseDouble(argument));
	}

}
