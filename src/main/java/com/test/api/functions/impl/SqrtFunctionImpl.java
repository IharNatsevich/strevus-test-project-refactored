package com.test.api.functions.impl;

import com.test.api.Function;

public class SqrtFunctionImpl implements Function {

	@Override
	public Double calculate(String argument) {

		return Math.sqrt(Double.parseDouble(argument));
	}

}
