package com.test.api.factory.impl;

import com.test.api.Factory;
import com.test.api.Operation;
import com.test.api.operations.impl.DivOperationImpl;
import com.test.api.operations.impl.MultOperationImpl;
import com.test.api.operations.impl.PowOperationImpl;
import com.test.api.operations.impl.SubOperationImpl;
import com.test.api.operations.impl.SummOperationImpl;
import com.test.utils.Constants;

public class OperationsFactoryImpl implements Factory<Operation> {

	@Override
	public Operation getItem(String opToken) {
		Operation operation = null;

		if (Constants.getSummToken().equals(opToken)) {
			operation = new SummOperationImpl();
		}
		if (Constants.getSubToken().equals(opToken)) {
			operation = new SubOperationImpl();
		}
		if (Constants.getMultToken().equals(opToken)) {
			operation = new MultOperationImpl();
		}
		if (Constants.getDivToken().equals(opToken)) {
			operation = new DivOperationImpl();
		}
		if (Constants.getPowToken().equals(opToken)) {
			operation = new PowOperationImpl();
		}

		return operation;
	}

}
