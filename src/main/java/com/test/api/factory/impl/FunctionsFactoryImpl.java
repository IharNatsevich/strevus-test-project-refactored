package com.test.api.factory.impl;

import com.test.api.Factory;
import com.test.api.Function;
import com.test.api.functions.impl.CosFunctionImpl;
import com.test.api.functions.impl.LogFunctionImpl;
import com.test.api.functions.impl.SinFunctionImpl;
import com.test.api.functions.impl.SqrtFunctionImpl;
import com.test.api.functions.impl.TgFunctionImpl;
import com.test.utils.Constants;

public class FunctionsFactoryImpl implements Factory<Function> {

	@Override
	public Function getItem(String funcToken) {
		Function function = null;

		if (Constants.getSqrtToken().equals(funcToken)) {
			function = new SqrtFunctionImpl();
		}
		if (Constants.getTgToken().equals(funcToken)) {
			function = new TgFunctionImpl();
		}
		if (Constants.getCosToken().equals(funcToken)) {
			function = new CosFunctionImpl();
		}
		if (Constants.getLogToken().equals(funcToken)) {
			function = new LogFunctionImpl();
		}
		if (Constants.getSinToken().equals(funcToken)) {
			function = new SinFunctionImpl();
		}

		return function;
	}

}
