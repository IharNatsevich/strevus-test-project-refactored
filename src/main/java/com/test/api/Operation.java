package com.test.api;

public interface Operation {

	public Double calculate(String op1, String op2);

}
