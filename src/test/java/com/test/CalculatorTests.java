package com.test;

import org.junit.Test;
import static org.junit.Assert.*;
import com.test.app.work.Calculator;
import com.test.app.work.Parser;


public class CalculatorTests {
	@Test
	public void testCalculate(){
		Parser parser = new Parser();
		parser.parseToRPN("26*8/4");
		Calculator calculator = new Calculator();
		 calculator.setOperationStack(parser.getOperationStack());
         calculator.setOutPutString(parser.getOutPutString());
         String result = String.valueOf(calculator.calculate());
         assertEquals("52.0", result);
	}
	@Test
	public void testDivideByZero(){
		Parser parser = new Parser();
		parser.parseToRPN("12/0");
		Calculator calculator = new Calculator();
		 calculator.setOperationStack(parser.getOperationStack());
         calculator.setOutPutString(parser.getOutPutString());
         String result = String.valueOf(calculator.calculate());
         assertEquals("Infinity", result);
	}

}
