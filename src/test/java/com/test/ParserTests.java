package com.test;
import org.junit.Test;

import com.test.app.work.Parser;


public class ParserTests {
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArgument() {
		Parser parser = new Parser();
		parser.parseToRPN("10!!!(cos(45)");
	}
	

}
