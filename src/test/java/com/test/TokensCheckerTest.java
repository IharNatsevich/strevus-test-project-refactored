package com.test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import com.test.app.work.TokensChecker;

public class TokensCheckerTest {

	@Test
	public void testIsNumber() {
		TokensChecker tokensChecker = new TokensChecker();
		assertTrue(tokensChecker.isNumber("5"));
	}

	@Test
	public void testIsNotNumber() {
		TokensChecker tokensChecker = new TokensChecker();
		assertFalse(tokensChecker.isNumber("tg"));
	}

	@Test
	public void testIsFunction() {
		TokensChecker tokensChecker = new TokensChecker();
		assertTrue(tokensChecker.isFunction("cos"));

	}

	@Test
	public void testIsNotFunction() {
		TokensChecker tokensChecker = new TokensChecker();
		assertFalse(tokensChecker.isFunction("+"));

	}

	@Test
	public void testIsOperations() {
		TokensChecker tokensChecker = new TokensChecker();
		assertTrue(tokensChecker.isOperation("+"));

	}

	@Test
	public void testIsNotOperations() {
		TokensChecker tokensChecker = new TokensChecker();
		assertFalse(tokensChecker.isOperation("cos"));

	}

	@Test
	public void testIsOpenBracket() {
		TokensChecker tokensChecker = new TokensChecker();
		assertTrue(tokensChecker.isOpenBracket("("));

	}

	@Test
	public void testIsNotOpenBracket() {
		TokensChecker tokensChecker = new TokensChecker();
		assertFalse(tokensChecker.isOpenBracket("*"));

	}

	@Test
	public void testIsCloseBracket() {
		TokensChecker tokensChecker = new TokensChecker();
		assertTrue(tokensChecker.isCloseBracket(")"));

	}

	@Test
	public void testIsNotCloseBracket() {
		TokensChecker tokensChecker = new TokensChecker();
		assertFalse(tokensChecker.isCloseBracket("*"));

	}

	@Test
	public void testIsFunctionExist() {
		TokensChecker tokensChecker = new TokensChecker();
		ArrayList<String> outputString = new ArrayList<String>();
		outputString.add("tg");
		assertTrue(tokensChecker.isFunctionExist(outputString));

	}
	@Test
	public void testIsFunctionNotExist() {
		TokensChecker tokensChecker = new TokensChecker();
		ArrayList<String> outputString = new ArrayList<String>();
		outputString.add("");
		assertFalse(tokensChecker.isFunctionExist(outputString));

	}
	@Test
	public void testIsOperationExist() {
		TokensChecker tokensChecker = new TokensChecker();
		ArrayList<String> outputString = new ArrayList<String>();
		outputString.add("*");
		assertTrue(tokensChecker.isOperationsExist(outputString));

	}
	@Test
	public void testIsOperationNotExist() {
		TokensChecker tokensChecker = new TokensChecker();
		ArrayList<String> outputString = new ArrayList<String>();
		outputString.add("");
		assertFalse(tokensChecker.isOperationsExist(outputString));

	}
	@Test
	public void testIsPriorityHigher(){
		TokensChecker tokensChecker = new TokensChecker();
		assertTrue(tokensChecker.isPriorityHigher(3,2));
	}
	@Test
	public void testIsNotPriorityHigher(){
		TokensChecker tokensChecker = new TokensChecker();
		assertFalse(tokensChecker.isPriorityHigher(2,3));
	}


}
